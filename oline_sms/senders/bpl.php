<?php

class bpl extends AktuelSms implements SmsSenderInterface
{
    public function __construct($message, $gsmnumber)
    {
        $this->message = $this->utilmessage($message);
        $this->gsmnumber = $this->utilgsmnumber($gsmnumber);
    }

    public function send()
    {
        if ($this->gsmnumber == "numbererror") {
            $log[] = ("Number format error." . $this->gsmnumber);
            $error[] = ("Number format error." . $this->gsmnumber);
            return null;
        }
        $params = $this->getParams();


        $url = "http://180.210.190.230:8228/httpapi/sendsms?userId=$params->user&password=$params->pass&smsText=" . urlencode($this->message) . "&commaSeperatedReceiverNumbers=$this->gsmnumber";

        $log[] = "Request url: " . $url;
        $result = simplexml_load_file($url);


        return array(
            'log' => $log,
            'error' => $error,
            'msgid' => $msgid,
        );
    }

    public function balance()
    {
        return null;
    }

    public function report($msgid)
    {
        return null;
    }


    public function utilgsmnumber($number)
    {
        return $number;
    }

    public function utilmessage($message)
    {
        return $message;
    }
}

return array(
    'value' => 'bpl',
    'label' => 'MiM SMS (BPL)',
    'fields' => array(
        'user', 'pass'
    )
);
