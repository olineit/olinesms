<?php
/* WHMCS SMS Addon by Oline SMS with GNU/GPL Licence
 * Oline SMS Host - http://www.olineit.com
 *
 * https://gitlab.com/olineit
 *
 * Developed Kazi Sala Uddin (http://www.amisalabir.org)
 * Licence: GPLv3 (http://www.gnu.org/licenses/gpl-3.0.txt)
 * */
if (!defined("WHMCS"))
    die("This file cannot be accessed directly");

require_once("smsclass.php");
require_once("senders/SmsSenderInterface.php");
$class = new AktuelSms();
$hooks = $class->getHooks();

foreach ($hooks as $hook) {
    add_hook($hook['hook'], 1, $hook['function'], "");
}